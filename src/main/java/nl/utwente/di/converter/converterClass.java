package nl.utwente.di.converter;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** AAAAAA
 */

public class converterClass extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Converter converter;
	
    public void init() throws ServletException {
    	converter = new Converter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Converter from Celsius to Fahrenheit";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius temperature: " +
                   request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit temperature: " +
                   Double.toString(converter.getFahrenheitTemperature(Double.parseDouble(request.getParameter("celsius")))) +
                "</BODY></HTML>");
  }
  

}
