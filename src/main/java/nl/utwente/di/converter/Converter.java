package nl.utwente.di.converter;


public class Converter {
    double getFahrenheitTemperature(double celsius){
        double fahrenheit = celsius * 1.8 + 32;
        return  fahrenheit;
    }

}
